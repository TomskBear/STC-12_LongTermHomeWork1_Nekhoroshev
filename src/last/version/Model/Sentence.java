package last.version.Model;

import last.version.Model.Exceptions.IncorrectSentenceEndException;
import last.version.Model.Exceptions.NullValueException;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private String originalSentense;
    private String noPunctuationString;

    private String removePunctuationRegexString = "[.,!?\\-()]";
    public Sentence(String originalSentense) throws NullValueException, IncorrectSentenceEndException {
        if (originalSentense == null) {
            throw new NullValueException("sentence");
        }

        if (!SentenceEndingChars.isEndOfSentence((originalSentense.charAt(originalSentense.length()-1)))) {
            throw new IncorrectSentenceEndException();
        }

        this.originalSentense = originalSentense;
        this.noPunctuationString = this.originalSentense.replaceAll(removePunctuationRegexString, "");
    }

    public String getOriginalSentenseText() {
        return originalSentense;
    }

    public boolean containsWord(String word) {
        return noPunctuationString.contains(word);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Sentence{");
        builder.append(originalSentense);
        builder.append(" }");
        return builder.toString();
    }
}
