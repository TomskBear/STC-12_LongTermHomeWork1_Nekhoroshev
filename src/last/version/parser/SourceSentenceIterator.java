package last.version.parser;

import last.version.Model.Exceptions.IncorrectSentenceEndException;
import last.version.Model.Exceptions.NullValueException;
import last.version.Model.Sentence;
import last.version.Model.SentenceEndingChars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class SourceSentenceIterator {

    private final String pathToSource;
    BufferedReader bufferedReader;

    public SourceSentenceIterator(String pathToSource) throws IOException {
        this.pathToSource = pathToSource;
        URL sourceUrl = new URL(pathToSource);
        bufferedReader = new BufferedReader(new InputStreamReader(sourceUrl.openStream()));
    }

    public void close(){
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Sentence getNextSentence() {
        StringBuilder wholeDataBuilder = new StringBuilder();
        int currentCharacterCode;
        try {
            currentCharacterCode = bufferedReader.read();
            while (currentCharacterCode != -1) {
                if ((char)currentCharacterCode != '\r' && (char)currentCharacterCode != '\n') {
                    wholeDataBuilder.append((char)currentCharacterCode);
                }
                int nextCharacterCode = bufferedReader.read();

                if(SentenceEndingChars.isEndOfSentence((char)currentCharacterCode)) {
                    while(SentenceEndingChars.isEndOfSentence((char)nextCharacterCode)) {
                        nextCharacterCode = bufferedReader.read();
                    }
                    break;
                }
                currentCharacterCode = nextCharacterCode;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (wholeDataBuilder.length() == 0) {
            return null;
        }

        Sentence sentence = null;
        try {
            sentence = new Sentence(wholeDataBuilder.toString());

        } catch (NullValueException e) {
            e.printStackTrace();
        } catch (IncorrectSentenceEndException e) {
            e.printStackTrace();
        }
        return sentence;
    }
}
