package last.version.forkJoinEngine;

import last.version.Model.Sentence;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ForkJoinTask;

public class SearchWordsInSentenseThread extends Thread {

    private ConcurrentLinkedQueue<String> resultList;
    private ConcurrentLinkedQueue<Sentence> sentencesForSearchQueue;
    private String[] words;
    private ForkJoinTask referenceTask;

    public SearchWordsInSentenseThread(
            ConcurrentLinkedQueue<Sentence> sentencesForSearchQueue,
            String[] words,
            ConcurrentLinkedQueue<String> resultList,
            ForkJoinTask referenceTask) {
        this.sentencesForSearchQueue = sentencesForSearchQueue;
        this.resultList =resultList;
        this.words = words;
        this.referenceTask = referenceTask;
    }

    @Override
    public void run() {

        System.out.println("start search of sentences");
        while(!referenceTask.isDone()) {
            Sentence sentence = sentencesForSearchQueue.poll();
            if (sentence != null) {
                for (String word : words) {
                    if (sentence.containsWord(word)) {
                        resultList.add(sentence.getOriginalSentenseText());
                        break;
                    }
                }
            }

        }
        System.out.println("FINISH search of sentences");
    }
}