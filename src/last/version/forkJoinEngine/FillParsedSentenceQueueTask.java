package last.version.forkJoinEngine;

import last.version.Model.Sentence;
import last.version.parser.SourceSentenceIterator;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.RecursiveAction;

public class FillParsedSentenceQueueTask extends RecursiveAction {

    private static final Integer THRESHOLD = 200;

    private ConcurrentLinkedQueue<Sentence> sentenceForSearchQueue;
    private String[] sources;
    private Integer start;
    private Integer end;

    public FillParsedSentenceQueueTask(String[] sources, Integer start, Integer end, ConcurrentLinkedQueue<Sentence> sentenceForSearchQueue) {
        this.sentenceForSearchQueue = sentenceForSearchQueue;
        this.sources = sources;
        this.start = start;
        this.end = end;
    }

    @Override
    protected void compute() {
        if (end - start > THRESHOLD) {
            int middle = (end + start) / 2;

            FillParsedSentenceQueueTask firstSubTask = new FillParsedSentenceQueueTask(sources, start, middle, sentenceForSearchQueue);
            FillParsedSentenceQueueTask secondSubTask  = new FillParsedSentenceQueueTask(sources, middle + 1, end,sentenceForSearchQueue);

            secondSubTask.fork();
            firstSubTask.fork();

            secondSubTask.join();
            firstSubTask.join();

        } else {
            for (int i = start; i < end; i++) {
                try {
                    SourceSentenceIterator iterator = new SourceSentenceIterator(sources[i]);
                    Sentence sentence;
                    while((sentence = iterator.getNextSentence()) != null) {
                        sentenceForSearchQueue.add(sentence);
                    }
                    iterator.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
