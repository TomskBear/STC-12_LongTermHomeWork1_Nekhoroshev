package last.version.implementation;

import last.version.Model.Sentence;
import last.version.forkJoinEngine.FillParsedSentenceQueueTask;
import last.version.forkJoinEngine.SaveFoundSentensesThread;
import last.version.forkJoinEngine.SearchWordsInSentenseThread;
import last.version.interfaces.IOccurenciesGetter;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ForkJoinPool;

public class OccurenciesGetter implements IOccurenciesGetter {
    @Override
    public void getOccurencies(String[] sources, String[] words, String res) {
        ForkJoinPool pool = new ForkJoinPool();
        ConcurrentLinkedQueue<Sentence> sentencesForSearchQueue = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<String> foundSentences = new ConcurrentLinkedQueue<>();
        FillParsedSentenceQueueTask fillSentenseQueueTask = new FillParsedSentenceQueueTask(sources, 0, sources.length, sentencesForSearchQueue);
        SearchWordsInSentenseThread searchWordsInSentenseThread = new SearchWordsInSentenseThread(sentencesForSearchQueue, words, foundSentences, fillSentenseQueueTask);
        searchWordsInSentenseThread.setDaemon(true);
        SaveFoundSentensesThread savingThread = new SaveFoundSentensesThread(res, foundSentences, fillSentenseQueueTask);
        savingThread.setDaemon(true);
        pool.execute(fillSentenseQueueTask);
        /*pool.execute(searchWordsInSentenseThread);
        pool.execute(savingThread);*/
        searchWordsInSentenseThread.start();
        savingThread.start();
        fillSentenseQueueTask.join();
        try {
            searchWordsInSentenseThread.join();
            savingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
