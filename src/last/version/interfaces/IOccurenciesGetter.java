package last.version.interfaces;

public interface IOccurenciesGetter {
    void getOccurencies(String[] sources, String[] words, String res);
}
